import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupOtpPage } from './signup-otp';

@NgModule({
  declarations: [
    SignupOtpPage,
  ],
  imports: [
    IonicPageModule.forChild(SignupOtpPage),
  ],
})
export class SignupOtpPageModule {}
