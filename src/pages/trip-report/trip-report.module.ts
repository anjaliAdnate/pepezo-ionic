import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TripReportPage } from './trip-report';
import { SelectSearchableModule } from 'ionic-select-searchable';

@NgModule({
  declarations: [
    TripReportPage,
  ],
  imports: [
    IonicPageModule.forChild(TripReportPage),
    SelectSearchableModule
  ],
})
export class TripReportPageModule {}
