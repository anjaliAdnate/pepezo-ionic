import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentGreetingPage } from './payment-greeting';

@NgModule({
  declarations: [
    PaymentGreetingPage,
  ],
  imports: [
    IonicPageModule.forChild(PaymentGreetingPage),
  ],
})
export class PaymentGreetingPageModule {}
