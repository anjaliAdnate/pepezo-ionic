import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddDevicesPage } from './add-devices';
import { SMS } from '@ionic-native/sms';
import { OnCreate } from './dummy-directive';

@NgModule({
  declarations: [
    AddDevicesPage,
    OnCreate
  ],
  imports: [
    IonicPageModule.forChild(AddDevicesPage)
  ],
  exports: [
    OnCreate
  ],
  providers: [
    SMS
  ]
})
export class AddDevicesPageModule {}
