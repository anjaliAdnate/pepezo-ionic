import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeviceSummaryRepoPage } from './device-summary-repo';
import { SelectSearchableModule } from 'ionic-select-searchable';
@NgModule({
  declarations: [
    DeviceSummaryRepoPage,
  ],
  imports: [
    IonicPageModule.forChild(DeviceSummaryRepoPage),
    SelectSearchableModule
  ],
})
export class DeviceSummaryRepoPageModule {}
